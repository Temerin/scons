/*
    Virtual Machine main

    This file is part of YLA VM (Yet another Language for Academic purpose: Virtual Machine).

    YLA VM is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Foobar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Foobar.  If not, see <http://www.gnu.org/licenses/>.

*/

#include "yla_vm.h"
#include "yla_test_gencode.h"
#include <string.h>
#include <stdio.h>

int main(int argpc, int argv)
{
    // VM
    yla_cop_type prg[HEADER_SIZE + 1];
    yla_cop_type *ptr = prg;

    put_header(&ptr, 0, 0, 1);
    put_commd(&ptr, CHALT);

    yla_vm vm;
    yla_vm_init(&vm, prg, HEADER_SIZE + 1);
    yla_vm_do_command(&vm);    

    printf("\n VM is running \n");
    yla_vm_done(&vm);
    printf("\n VM is closing \n");

    // Calc
   yla_stack stack;
    
    printf("\n Start Lex Calc \n");
    yla_stack_init(&stack, 100);
    calc(&stack);
    yla_stack_done(&stack);
    printf("\n Finish Lex Calc \n");
}
